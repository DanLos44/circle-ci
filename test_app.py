import unittest
from app import app

class TestApp(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.client = app.test_client()

    def test_test_endpoint(self):
        response = self.client.get('/test/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b'this is a test')

if __name__ == '__main__':
    unittest.main()

